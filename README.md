# How to Add New User on Ubuntu/Linux Mint (from Command Line)

Open a terminal.

    sudo adduser _new user username_

If you want to give the new user admin privilege,

    sudo visudo

Then, add the new username similar to "root".

Or, you can assign the admin privilege when creating the account.

    sudo adduser _new user username_ sudo


